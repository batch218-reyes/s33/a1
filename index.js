
fetch('https://jsonplaceholder.typicode.com/todos')
	.then(response => response.json())
	.then(array => console.log(array.map(item => item.title)));


fetch('https://jsonplaceholder.typicode.com/todos/1')
	.then(response => response.json())
	.then(json => console.log(`Thie item "${json.title}" on the list has a status of ${json.completed}.`));


//----------------------------------------------------


fetch('https://jsonplaceholder.typicode.com/todos', {
		method: 'POST',
		headers: {'Content-type' : 'application/json'},
		body: JSON.stringify({
				"title": 5,
	   			"title": "Created To Do List Item",
	   			"completed": false
			})
		})
	.then(response => response.json())
	.then(json => console.log(json))

//-------------------------------------------------------


fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {'Content-type' : 'application/json'},
	body: JSON.stringify({
	"userId": 1,
    "title": "Updated To Do List Item",
    "description" : "To update the my to do list with a different data structure.",
    "status": "Pending",
    "dateCompleted" : "Pending"
	})
})
.then(response => response.json())
.then(json => console.log(json))

//----------------------------------------------------


fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {'Content-type' : 'application/json'},
	body: JSON.stringify({
    status: "Complete",
    "dateCompleted": "01/12/21"
	})
})
	.then(response => response.json())
	.then(json => console.log(json))

//------------------------------------------------------


fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: "DELETE",
})
.then((response)=>response.json())
.then((json)=> console.log(json));
